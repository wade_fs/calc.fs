## Abort

``` 
Abort()
``` 

> 完全停止運算並返回 `$Aborted`.

### 範例
``` 
>> Print("a"); Abort(); Print("b")
$Aborted
``` 
 
